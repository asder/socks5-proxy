FROM node:16 as builder

RUN mkdir -p /usr/src/

WORKDIR /usr/src/

COPY package*.json ./


RUN npm ci

COPY . .

ENTRYPOINT ["node", "index.js"]